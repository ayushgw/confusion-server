## Confusion Server

This is meant to create json server for [Confusion React App](https://bitbucket.org/ayushgw/confusion-react-app/src/master/).

In the project directory, navigate to /json-server and then you can run:

### `json-server --watch db.json`

Runs the server<br>
Open Confusion App in another window to view it in browser.


**Note: server url in the /src/shared/baseUrl.js in confusion-react-app must match the json-server url.**
